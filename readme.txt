=== Crayon Syntax Highlighter to Pastacode ===
Contributors: willybahuaud
Tags: pastacode, crayon, syntax, highlighter, migration
Requires at least: 3.1
Tested up to: 4.6
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The only use of this plugin is to convert Crayon Syntax Highlighter's tags into Pastacode shortcodes.

== Description ==

The only use of this plugin is to convert [Crayon Syntax Highlighter](https://fr.wordpress.org/plugins/crayon-syntax-highlighter/)'s tags into [Pastacode](https://fr.wordpress.org/plugins/pastacode/) shortcodes.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Launch tags conversion in submenu Tools > Migrate to Pastacode

== Changelog ==

= 1.0 =
* Initial release
